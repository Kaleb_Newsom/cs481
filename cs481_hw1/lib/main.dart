import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
          appBar: AppBar(
            title: Text('Welcome to Flutter'),
          ),
          body: Center(
            child: Text('''
            Hello World
            My Name is Kaleb Newsom

            Favorite Quote:
            "Life isn't about finding yourself
            Life is about creating yourself"
            George Bernard Shaw
            '''),
          ),
    ),
    );
  }
}
