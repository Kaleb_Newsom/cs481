import 'package:flutter/material.dart';
import 'initialpage.dart';
import 'tomorrow.dart';

void main() {
  runApp(MaterialApp(
      routes: {
        '/': (context) => InitialPage(),   // home screen
        '/tomorrow' : (context) => Tomorrow(),
      }
  ));
}
