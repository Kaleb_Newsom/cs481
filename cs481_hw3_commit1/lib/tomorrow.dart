import 'package:flutter/material.dart';

class Tomorrow extends StatefulWidget {
  // This widget is the root of your application.
  // like a main function that returns a Widget tree called a MaterialApp
  @override
  _TomorrowState createState() => _TomorrowState();
}

class _TomorrowState extends State<Tomorrow> {
  int likedCount = 0;
  @override
  Widget build(BuildContext context) {
    Color color = Theme
        .of(context)
        .primaryColor;
    Widget titleSection = Container( // containers can only have a child
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [ // so Row widgets can have children
          Expanded( // widget that expands to fill in the space
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // aligned to the left
              children: [ // so Column widgets can have children
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('If the universe is within you as Rumi suggests...',
                    style: TextStyle(fontWeight: FontWeight.bold,), // TextStyle
                  ),
                ),
                Text('Then we must be more than this physical body.', style: TextStyle(
                  color: Colors.grey[900],
                  fontSize: 16,),
                ),
              ],
            ),
          ),
        ],
      ),
    );

// a defined widget tree
    Widget buttonSection = Container( // containers can only have a child
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildButtonColumn('Assets/quietude.jpg', color, ''), // function calls
        ],
      ),
    );

    // a defined widget tree
    Widget textSection = Container(
      padding: const EdgeInsets.all(16),
      child: Text(
        '     Normally, when we think of the universe, at least here in the west,'
            ' we take the reductionist perspective of dividing reality into separate'
            ' parts where each part we give a name.  Even though this is a useful tool,'
            ' I ask the question..Whoever told us to divide reality?'
            ' In Zen Buddhism, there is a saying, \"A finger may be needed to point'
            ' to the moon, but once the moon is found, worry not of the finger.\"'
            ' Likewise, even though a name may be necessary to focus on an aspect of'
            ' reality, ultimately everything is nameless and without division--just'
            ' as it\'s always been.  Remember, you exist simultaneously with everything'
            ' else.'
            '\n     Throughout our lives, we fall under the spell of all of the divisions that we\'ve made.'
            ' Because of this, we feel separate from everything and everyone. And yet,'
            ' this way of seeing does not honor what Buddhists might call the \"original'
            ' mind\" or first way of seeing the universe--as in before any divisions were made,'
            ' within the complete quietude of the mind\'s activities.  When we see separation, we are simply'
            ' observing the waves of our mind and not the original form of seeing. ',
        softWrap: true, // the wrap is not necessarily per line, but based on the phone screen
      ),
    );



    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[800],
          title: Center(
              child: Text('Daily Zen',)),
        ),
        body: ListView(
            children: [
              Image.asset(
                'Assets/universeIsWithin.jpg',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              titleSection, // places these widget subtrees into the tree
              textSection,
              //buttonSection,
              Image.asset(
                'Assets/quietude.jpg',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween, // use this for both
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.navigate_before),
                        iconSize: 30,
                        color: Colors.deepPurpleAccent[700],
                        alignment: Alignment.center,
                        onPressed: backPage,
                      ),
                    ),
                    /*
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.navigate_next),
                        iconSize: 30,
                        color: Colors.deepPurpleAccent[700],
                        alignment: Alignment.center,
                        onPressed: backPage,
                      ),
                    ),*/
                  ],
                ),
              ),
            ]
        ),
      ),

    );

  }
  /*
  void nextPage(){
    setState((){
      Navigator.pushReplacementNamed(context, '/example');
    });
  }
  */
  void backPage(){
    setState((){
      Navigator.pushReplacementNamed(context, '/');
    });
  }
}

// meant to include a text item and an icon
// when defining the Icons, you can use ctrl+space to browse through the list
// of available options(with previews!)
// used in the button section
// like a function that returns a Column widget
Column _buildButtonColumn(String relativeUrl, Color color, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Image.asset(
        relativeUrl,
        width: 150,
        height: 150,
        fit: BoxFit.cover,
      ),
      Container(
        margin: const EdgeInsets.only(top: 8),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400,
            color: Colors.deepPurpleAccent[700],
          ), // TextStyle
        ), // Text
      ), // Container
    ],
  );
}
