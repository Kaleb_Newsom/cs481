import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // like a main function that returns a Widget tree called a MaterialApp
  @override
  Widget build(BuildContext context) {
    Color color = Theme
        .of(context)
        .primaryColor;

    Widget titleSection = Container( // containers can only have a child
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [ // so Row widgets can have children
          Expanded( // widget that expands to fill in the space
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // aligned to the left
              children: [ // so Column widgets can have children
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('If so, it begs the question...',
                    style: TextStyle(fontWeight: FontWeight.bold,), // TextStyle
                  ),
                ),
                Text('Is it possible for the universe to become\naware of itself?', style: TextStyle(
                  color: Colors.grey[900],), // TextStyle
                ),
              ],
            ),
          ),
          Icon(Icons.all_inclusive, color: Colors.deepPurple[500],),
          //Text('11'),
        ],
      ),
    );

// a defined widget tree
    Widget buttonSection = Container( // containers can only have a child
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.sentiment_very_satisfied, 'Be'), // function calls
          _buildButtonColumn(color, Icons.wb_sunny, 'Yourself'),
          _buildButtonColumn(color, Icons.sync, 'Ouroboros'),
        ],
      ),
    );

    // a defined widget tree
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'For a long time, when I was younger, I practiced yoga and meditation.'
            '  I was always kind of a believe in what you can directly experience'
            'kind of person.  These two quotes, I believe, come across as a'
            ' naturalistic spiritual philosophy--that is, of nature, but without'
            ' the reductionist view of a pantheon of gods imbuing various aspects'
            ' of nature.  Instead, if we simply take what we can directly experience,'
             'that is, without any spiritual fluff, we can see that we exist'
             ' simultaneously with the whole at every moment.  What a mystery'
              ' this is!',
        softWrap: true, // the wrap is not necessarily per line, but based on the phone screen
      ),
    );
    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Daily Zen'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'Assets/coolQuotes.jpg',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              titleSection, // places these widget subtrees into the tree
              buttonSection,
              textSection
            ]
        ),
      ),

    );
  }
}

// meant to include a text item and an icon
// when defining the Icons, you can use ctrl+space to browse through the list
// of available options(with previews!)
// used in the button section
// like a function that returns a Column widget
  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ), // TextStyle
          ), // Text
        ), // Container
      ],
    );
  }
