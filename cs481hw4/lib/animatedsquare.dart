import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

class AnimatedSquare extends AnimatedWidget {
  // Make the Tweens static because they don't change.
  static final _opacityTween = Tween<double>(begin: 0.15, end: .6);
  static final _sizeTween = Tween<double>(begin: 18, end: 100);
  static final _rotationTween = Tween<double>(begin: 0, end: -70);

  AnimatedSquare({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Center(
          child: Transform.rotate(
            angle: _rotationTween.evaluate(animation),
            child: Container(
              child: Image.asset(
              'Assets/zenOuroborus.png',
              //margin: EdgeInsets.symmetric(vertical: 10),
              height: _sizeTween.evaluate(animation),
              width: _sizeTween.evaluate(animation),
              //color: Colors.pink,
                ),
            ),
          ),
        ),
      ),
    );
  }
}
