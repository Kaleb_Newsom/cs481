import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'animatedsquare.dart';

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with TickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;
  Animation<Offset> positionAnimation;
  AnimationController controller2;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 7), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.decelerate)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });
    controller.forward();

    controller2 = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: false);


    positionAnimation = Tween<Offset>(
      begin: const Offset(-4, 0),
      end: const Offset(4, 0),
    ).animate(CurvedAnimation(
      parent: controller2,
      curve: Curves.ease, //Curves.ease,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body:  Container(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SlideTransition(
                    position: positionAnimation,
                    child: ClipOval(
                      child: Image.asset(
                        'Assets/theFlash.jpg',
                        height: 60,
                        width: 60,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  AnimatedSquare(animation: animation),
                ]),
          ),
        ),

      );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
