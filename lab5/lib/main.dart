import 'package:flutter/material.dart';
import 'package:whiteboardkit/whiteboardkit.dart';
import 'dart:math';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // code from https://pub.dev/packages/whiteboardkit/example START
  DrawingController controllerA;
  DrawingController controllerB;
  DrawingController controllerC;

  @override
  void initState() {
    controllerA = new DrawingController();
    controllerA.onChange().listen((draw) {
      //do something with it
    });
    controllerB = new DrawingController();
    controllerB.onChange().listen((draw) {
      //do something with it
    });
    controllerC = new DrawingController();
    controllerC.onChange().listen((draw) {
      //do something with it
    });
    super.initState();
  }
  //code from https://pub.dev/packages/whiteboardkit/example END


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: DefaultTabController(
          length: 3,
          child:
          Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              bottom: TabBar(
                tabs: [
                  Tab(child: Text("Whiteboard 1"),),
                  Tab(child: Text("Whiteboard 2"),),
                  Tab(child: Text("Whiteboard 3"),),
                ],
              ),
              title: Center(
                  child: Text('Tabula Rasa',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Colors.purple[800],
                    ),)),
              actions: [
                PopupMenuButton(
                  icon: Icon(Icons.help_outline,
                    color: Colors.purple[800],
                  ),
                  itemBuilder: (context) =>[
                    PopupMenuItem(
                      child: Text(
                        "There are three different tabs representing three different whiteboards with which to draw on.\n\n" +
                            "When transitioning to another whiteboard (tab), you must press on the whiteboard with a selected painting tool to restore any" +
                            " previous work that was on the whiteboard.\n\n" +
                            "From left to right, the icons on the bottom of the screen are:\n" +
                            "1. Pen size selector\n" +
                            "2. Paint color selector\n" +
                            "3. Eraser size selector\n" +
                            "4. Clear Screen\n" +
                            "5. Undo\n\n" +
                            "The Scale Button on the bottom of the screen is used to scale the illustration\n\n" +
                            "When performing a long press on the draw area of any of the three whiteboards, a tooltip will popup which lets the user know that they can draw in that area."
                        ,),
                    ),
                  ],
                ),
              ],
            ),
            body: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                Column(
                  // code from https://pub.dev/packages/whiteboardkit/example START
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Tooltip(
                        message: "Draw something here",
                        child: Whiteboard(
                          controller: controllerA,
                        ),
                      ),
                    ),
                    // code from https://pub.dev/packages/whiteboardkit/example END
                    RaisedButton(
                      color: Colors.blueAccent,
                      onPressed: () => [increaseScaleA(), this.setState(() {})],
                      child: new Text('Increase scale'),
                    ),
                    RaisedButton(
                      color: Colors.blueAccent,
                      onPressed: () => [decreaseScaleA(), this.setState(() {})],
                      child: new Text('Decrease Scale'),
                    ),
                  ],
                ),
                Column(
                  // code from https://pub.dev/packages/whiteboardkit/example START
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Tooltip(
                          message: "Draw something here",
                          child: Whiteboard(
                            controller: controllerB,
                          ),
                        )

                    ),
                    // code from https://pub.dev/packages/whiteboardkit/example END
                    RaisedButton(
                      color: Colors.blueAccent,
                      onPressed: () => [increaseScaleB(), this.setState(() {})],
                      child: new Text('Increase scale'),
                    ),
                    RaisedButton(
                      color: Colors.blueAccent,
                      onPressed: () => [decreaseScaleB(), this.setState(() {})],
                      child: new Text('Decrease Scale'),
                    ),

                  ],
                ),
                Column(
                  // code from https://pub.dev/packages/whiteboardkit/example START
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Tooltip(
                        message: "Draw something here",
                        child: Whiteboard(
                          controller: controllerC,
                        ),
                      ),

                    ),
                    // code from https://pub.dev/packages/whiteboardkit/example END
                    RaisedButton(
                      color: Colors.blueAccent,
                      onPressed: () => [increaseScaleC(),this.setState(() {})],
                      child: new Text('Increase scale'),
                    ),
                    RaisedButton(
                      color: Colors.blueAccent,
                      onPressed: () => [decreaseScaleC(),this.setState(() {})],
                      child: new Text('Decrease Scale'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
    );
  }
  increaseScaleA()
  {
    controllerA.draw.height += 30;
    controllerA.draw.width += 30;
  }
  decreaseScaleA()
  {
    controllerA.draw.height -= 30;
    controllerA.draw.width -= 30;
  }
  increaseScaleB()
  {
    controllerB.draw.height += 30;
    controllerB.draw.width += 30;
  }
  decreaseScaleB()
  {
    controllerB.draw.height -= 30;
    controllerB.draw.width -= 30;
  }
  increaseScaleC()
  {
    controllerC.draw.height += 30;
    controllerC.draw.width += 30;
  }
  decreaseScaleC()
  {
    controllerC.draw.height -= 30;
    controllerC.draw.width -= 30;
  }

  // showDialog(
  //     context: context,
  //     builder: (BuildContext context){
  //       return alert;
  //     }
  // );

  @override
  void dispose() {
    controllerA.close();
    controllerB.close();
    controllerC.close();

    super.dispose();
  }
}